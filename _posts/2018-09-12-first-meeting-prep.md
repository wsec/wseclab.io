---
layout: post
title: Preparation for the first meeting!
bigimg: /img/anonymity-intensifies.jpg
tags: [meta]
---

Welcome to Wootton Cybersecurity Club! In this club, we will start off by learning the basics of the Linux command line interface. Then, we will move towards advanced cybersecurity concepts in preparation for **CTF competitions**. 

> A CTF, or Capture The Flag is a computer security competition in which teams (usually 2-5 people) work together to get the highest number of points. Points are awarded for completing challenges or tasks in a range of categories (cryptography, stegonography, binary analysis, reverse engineering, web based attacks, mobile security and others).

There will be several CTFs taking place throughout the year, and we will also host some ourselves. If you want to get started with competing, there is an online CTF taking place on **September 28, 2018** called [picoCTF](https://picoctf.com/). You can either work alone or with a team of up to 4 other people.

We will be having our first meeting on **Thursday, September 20, 2018 at lunch**, during which we will be **installing Kali Linux** on the machines in the Networking Lab **(In Room 15)**.

You may bring your own laptop, but this is not necessary.

If you have a spare USB drive, please image it and bring it to the meeting. First, download the [Kali Linux 64 Bit ISO](https://www.kali.org/downloads/) and then [follow this guide to image your USB](https://gitlab.com/wsec/club/wikis/Creating-a-bootable-Linux-live-disk-with-Rufus-on-Microsoft-Windows). Note that your drive should be at least 4 GB in capacity and this procedure will wipe its contents. Alternatively, you could download the Kali Linux Light 64 Bit ISO for flash drives with 1-4 GB capacity however this version is the bare minimum needed to run Kali and has less tools.

If you get stuck or don't have a drive, message one of us (Basaam, Shrikar, or Konstantin).
