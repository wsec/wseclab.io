---
layout: post
title: Interest Meeting
bigimg: /img/networking.jpg
---

Thank you for your interest in Cybersecurity Club! [Click here](/about/) to see more information about the club itself. If you haven't already, check the [facebook page](https://facebook.com/whscyber) for updates. An interest meeting will be held on Tuesday, June 12 at lunch.                  

Over the course of the following schoolyear, we will be covering operating the Linux **Command Line Interface (CLI)**, an introduction to advanced cybersecurity concepts, and general strategies for competing in **Capture the Flag (CTF)** competitions.

Meetings will be weekly, however, the day and time (either lunch or afterschool) will be announced at a later date. We will meet in **Room 15**, a computer lab with a door to another computer lab (networking lab) where we will be.

If you would like to bring your own laptop, feel free, though *this is optional.* 
