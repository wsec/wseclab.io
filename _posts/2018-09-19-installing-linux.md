---
layout: post
title: First Meeting
bigimg: /img/kali-simple.png
tags: [meta]
---
Thanks for joining Wootton Cybersecurity Club! Check the [about](/about/) section for information about the club.

# Introduction to Linux
*Linux* is an operating system, like Windows or macOS, that is based on [UNIX](http://www.ee.surrey.ac.uk/Teaching/Unix/unixintro.html). It was developed by Linus Torvalds in 1991 as a free operating system kernel.
Now, Linux is everywhere. On servers, supercomputers, and even the International Space Station. It is a free, **open-source**, and highly customizable [GNU](https://www.gnu.org/philosophy/philosophy.html) Project.

> Open source refers to any program whose source code is made available for use or modification as users or other developers see fit. Open source software is usually developed as a public collaboration and made freely available.

Various distributions, or **"distros"**, of GNU/Linux are actively developed by both communities and for-profit companies.

# Installing Linux

We will be **dual-booting** Kali Linux, meaning multiple operating systems will be installed on one computer, and you will be able to choose which one to boot into in the boot loader.
*GRUB2* is the most common Linux bootloader. After system initialization, the GRUB menu presents the user with a list of operating systems to boot into. 

Linux requires at least one partition to be mounted as `/` (root). An additional *swap space* can be created to handle system memory when the RAM is full or store a session during a "suspended" state (hibernation).
Read more on swap space here: [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/ch-swapspace](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/ch-swapspace).

First, you will need a bootable USB imaged with Kali Linux. To create one, start by downloading the [Kali Linux ISO](https://www.kali.org/downloads/) 
associated with your system architecture (64-Bit or 32-Bit). 
Then [follow this guide](https://gitlab.com/wsec/club/wikis/Creating-a-bootable-Linux-live-disk-with-Rufus-on-Microsoft-Windows) to image your USB.

Now you're ready to install Kali. Boot into the computer and immediately hold the respective key (usually F12 or F2) until the Boot Menu appears. 
Use the arrow keys to select the USB Flash Drive and hit enter. A menu for Kali Linux will appear. Select *Live*, and the machine will begin booting into Kali.
From here, use [this documentation](https://docs.kali.org/installation/dual-boot-kali-with-windows) to partition the hard drive for dual-booting.

# The File System Heirarchy

The Linux file system starts with the root (`/`) directory. All other directories are children of the root. 
The partition which the root file system resides on is mounted first during boot and the system will not boot if it doesn't find it.
The file system contains the following directories:

|Directory| Function                                         |
|--------|---------------------------------------------------|
| /bin   | Essential command binaries
| /boot  | Static files of the boot loader                   |
| /dev   | Device files                                      |
| /etc   | Host-specific system configuration                |
| /lib   | Essential shared libraries and kernel modules     |
| /media | Mount point for removable media                   |
| /mnt   | Mount point for mounting a filesystem temporarily |
| /opt   | Add-on application software packages              |
| /sbin  | Essential system binaries                         |
| /srv   | Data for services provided by this system         |
| /tmp   | Temporary files                                   |
| /usr   | Secondary hierarchy                               |
| /var   | Variable data                                     |

![filesystem](http://i.imgur.com/gtuIBzE.png)

# picoCTF

picoCTF is an online cybersecurity competition hosted by Carnegie Mellon University. It begins on **September 28** and ends on **October 12**. 
This is a good chance to get a feel of the types of challenges that a CTF has to offer. Teams consist of 1-5 people. 
We encourage you to find some teammates and then [register](https://picoCTF.com).

