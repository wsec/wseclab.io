---
layout: post
title: Interest Meeting Recap
bigimg: /img/lock.png
---

At the interest meeting, we discussed the plan for next school year. 

Initially, we will be teaching basic Linux commands, Linux system administration, as well as fundamental information about the operating system. Once a base is developed, we will move on to advanced cybersecurity concepts such as Man In The Middle (MitM) attacks, ARP Poisoning, and Packet Sniffing. The distributions of Linux that will be used are [Kali Linux](https://www.kali.org/) and [Parrot Security OS](https://www.parrotsec.org/). These are built for penetration testing.

> A penetration test, colloquially known as a pen test, is an authorized simulated attack on a computer system that looks for security weaknesses, potentially gaining access to the system’s features and data.

If you have prior knowledge of Linux, we will offer more advanced concepts and CTF preparation. 

Throughout the course of the year, we will search for Capture the Flag challenges and hackathons that you can participate in as well as set up some practice activities ourselves. In preparation, we will also cover Reverse Engineering, Binary Exploitation, Cryptography, Web Exploitation, and strategies for competing. 

Our first meeting next year will take place after the activity fair. Until then, if you would like to get a head start, you can learn independently to be introduced to Linux and become familiar with it. I would recommend looking at the "Getting Started" tab on the club webite, starting with [this tutorial](https://www.funtoo.org/Linux_Fundamentals,_Part_1), and referring to [https://gitlab.com/wsec/club/wikis/Basic-UNIX-and-bash-commands](https://gitlab.com/wsec/club/wikis/Basic-UNIX-and-bash-commands) to learn some basic commands.

To dual-boot Linux from a USB Drive on your computer, first download an ISO image of the desired distribution (usually found on their websites) then follow [this guide](https://gitlab.com/wsec/club/wikis/Creating-a-bootable-Linux-live-disk-with-Rufus-on-Microsoft-Windows). If you are unsure which distribution you want, just use Elementary OS ([https://elementary.io/](https://elementary.io/)), which is based on Ubuntu. Don't worry if you have any trouble; we will be installing Linux on the school computers during the first meeting.

Enjoy your summer!