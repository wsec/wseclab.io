---
layout: post
title: Preparation for the first meeting!
bigimg: /img/anonymity-intensifies.jpg
tags: [meta]
---

Welcome to Wootton Cybersecurity Club! In this club, we will be...

We will be having our first meeting on **TBD**, during which we will be **installing Kali Linux** on the machines in the Networking Lab **(In Room 15)**.

You may bring your own laptop, but this is not necessary.

If you have a spare USB drive, please image it and bring it to the meeting. First, download the [Kali Linux 64 Bit ISO](https://www.kali.org/downloads/) and then [follow this guide to image your USB](https://gitlab.com/wsec/club/wikis/Creating-a-bootable-Linux-live-disk-with-Rufus-on-Microsoft-Windows). Note that your drive should be at least 4 GB in capacity and this procedure will wipe its contents.

If you get stuck or don't have a drive, message one of us (Basaam, Shrikar, or Konstantin).
